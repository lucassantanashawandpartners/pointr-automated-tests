// example of a test group
// note: all tests under the test directory are ran


describe('Registration And Sign In automated tests', ()=> {


	describe('Registering tests', () => {

		// it('Should register using email and log out', () => {
		//
		//     browser.url('http://dev.pointr.co');
		//
		//     //Clicks the join button
		//     const joinBtn = $('.sc-bdVaJa.fXujtA');
		//     joinBtn.click();
		//
		//
		// 	//Checks if in proper URL.
		// 	const registerURL = browser.getUrl();
		// 	expect(registerURL).to.equal('https://dev.pointr.co/register');
		//
		//
		//     //Fetches emailBtn class and clicks it
		//     const emailBtn = $$('.sc-htoDjs.hftcWM')[3];
		//     emailBtn.click();
		//
		//     //Form appears, elements stored
		// 	const form = $('.sc-gqPbQI.LjxYJ');
		//     const firstNameField = $$('input.sc-iwsKbI.gJVNEa')[0];
		//     const lastNameField = $$('input.sc-iwsKbI.gJVNEa')[1];
		//     const emailField = $$('input.sc-iwsKbI.gJVNEa')[2];
		//     const passwordField = $$('input.sc-iwsKbI.gJVNEa')[3];
		//     const passwordConfirmationField = $$('input.sc-iwsKbI.gJVNEa')[4];
		//
		//
		//     //Setting values
		// 	firstNameField.setValue('Lucas');
		// 	lastNameField.setValue('Santana');
		//     emailField.setValue('ned_jakubowski@putsbox.com');
		//     passwordField.setValue('automatedTest1');
		//     passwordConfirmationField.setValue('automatedTest1');
		//
		//
		//     expect(firstNameField.getValue()).to.equal('Lucas');
		//     expect(lastNameField.getValue()).to.equal('Santana');
		//     expect(emailField.getValue()).to.equal('ned_jakubowski@putsbox.com');
		//     expect(passwordField.getValue()).to.equal('automatedTest1');
		//     expect(passwordConfirmationField.getValue()).to.equal('automatedTest1');
		//
		//
		//    //Submits form
		// 	form.submitForm();
		//
		//    //If form is submitted, correctly, billboard renders.
		// 	browser.waitForExist('.sc-bxivhb.cTKtsz',5000);
		//
		//
		//    // Now, logging out:
		//
		// 	const HiFirstNameBtn = $('.sc-bxivhb.cTKtsz');
		//
		// 	HiFirstNameBtn.waitForExist();
		// 	HiFirstNameBtn.click();
		//
		// 	const signOutBtn = HiFirstNameBtn.$('.sc-bZQynM.bMmSDl');
		// 	signOutBtn.click();
		//
		// });

		// it('Should register using google account', () => {
		//
		// });
		//
		// it('Should register using facebook account', () => {
		//
		// });
		//
		// it('Should register using linkedIn account', () => {
		//
		// });
	});


	describe('Sign in, sign out and password tests', () => {

		it('Should reset password, check email link and login', ()=>{

			browser.url('http://dev.pointr.co');

			const signIn = $$('.sc-ckVGcZ.jfwlMz')[4].$('a');
			signIn.click();

			//Useful fields in this signInModal
			const form = $('form');
			const forgotPassBtn = form.$('a.sc-eLExRp.hstEny');

			//Clicks forgotPassBtn
			forgotPassBtn.click();

			//Filling fields
			const modalResetPass = $('.sc-fYiAbW.bBnauJ');
			expect(modalResetPass).to.exist;

			const resetPassInput = modalResetPass.$('form').$('input');
			const resetPassBtn = modalResetPass.$('button');

			resetPassInput.setValue('ned_jakubowski@putsbox.com');
			resetPassBtn.click();

			//Open email. putsbox made this very easy;

			browser.pause(3000);

			browser.url('https://preview.putsbox.com/p/ned_jakubowski/last');


			//Is it the right email? Emails on putbox expires within 15 minutes
			expect(browser.getTitle()).to.equal('Please reset your password');

			//Filtering putsbox email to get the proper link
			const resetLink = $('body')
				.getText()
				.replace('We heard that you lost your Pointr password. Sorry about that!', '')
				.replace('But don’t worry! You can use the following link to reset your password:', '')
				.replace('If you don’t use this link within 3 hours, it will expire.', '')
				.replace('Don\'t like these emails? Unsubscribe.','')
				.replace('Powered by Pointr.', '')
				.replace(' ', '');

			browser.url(resetLink);

			browser.pause(2000);

			//Is it in the Pointr page? On reset password page?
			expect(browser.getTitle()).to.equal('Pointr');
			expect(browser.getUrl()).to.include('https://dev.pointr.co/password_reset/');

			const newPassForm = $('.sc-eilVRo.gpmZWU');
			const newPassField = newPassForm.$$('input')[0];
			const newPassConfirmationField = newPassForm.$$('input')[1];
			const submitBtn = newPassForm.$('button');

			newPassField.setValue('automatedTest1');
			newPassConfirmationField.setValue('automatedTest1');
			submitBtn.click();

			//Now, has to login with the new password
			browser.pause(1000);

			const signInModalEmailInput = $('form').$$('input')[0];
			const signInModalPassInput = $('form').$$('input')[1];
			const signInBtn = $('form').$('button');

			signInModalEmailInput.setValue('ned_jakubowski@putsbox.com');
			signInModalPassInput.setValue('automatedTest1');
			signInBtn.click();

			//
			browser.pause(3000);
			const questionsURL = browser.getUrl();
			expect(questionsURL).to.equal('https://dev.pointr.co/questions');

			// Now it's time to sign out.


			//"Hi, First Name"
			const HiFirstNameBtn = $('.sc-bxivhb.cTKtsz');
			HiFirstNameBtn.click();


			//sign out button appears, clicks
			const signOutBtn = $('.sc-bZQynM.bMmSDl');
			signOutBtn.click();


			//Sign In tab must be open: Modal exists and it is in the right URL.

			const signInURL = browser.getUrl();
			expect(signInURL).to.equal('https://dev.pointr.co/register');

		});

		it('should log in credentials saved and log out', ()=> {

			//It's already logged in, in the previous test.
			//Revisiting the site
			browser.url('http://dev.pointr.co');

			const signIn = $$('.sc-ckVGcZ.jfwlMz')[4].$('a');
			signIn.click();

			const redirectBtn = $('.sc-jTzLTM.DCWLR');
			redirectBtn.click();


			const HiFirstNameDiv = $$('.sc-dxgOiQ.iceoom')[1];

			// Now it's time to sign out.

			browser.debug();
		});


		// it('should sign in and out using email (web app)', () => {
		//
		// 	browser.url('http://dev.pointr.co');
		//
		//
		// 	//Sign In part
		//
		// 	//Clicks on signInBtn
		// 	const signIn = $$('.sc-ckVGcZ.jfwlMz')[4].$('a');
		// 	signIn.click();
		//
		//
		// 	//Modal appears. Gets
		//
		// 	const signInModal = $('.sc-fjdhpX.hOPgJl');
		// 	//Modal must exist
		// 	expect(signInModal).to.exist;
		//
		// 	//Fetching fields to be used;
		// 	const emailField = $$('.sc-iwsKbI.kNWfOS')[0];
		// 	const passwordField = $$('.sc-iwsKbI.kNWfOS')[1];
		// 	const form = $('form');
		// 	const submitBtn = form.$('button');
		// 	const forgotPassBtn = form.$('a.sc-eLExRp.hstEny');
		// 	const registerBtn = signInModal.$('.sc-krvtoX.gLVusr');
		//
		// 	//Disabled button (will have to check if it's grey)
		// 	expect(!!submitBtn.getAttribute('disabled')).to.equal(true);
		//
		// 	//This one is tricky: getCssProperty.value returns rgba on chrome and rgb on firefox.
		// 	//A way I found is to check if the string includes the '205,205,205' string, which is grey
		// 	expect(submitBtn.getCssProperty('background-color').value).to.include('205,205,205');
		//
		// 	//Checks if forgotPassBtn has the right text and registerBtn the right link.
		// 	expect(forgotPassBtn.getText()).to.equal('Forgot password?');
		// 	expect(registerBtn.getAttribute('href')).to.equal('https://dev.pointr.co/register');
		//
		//
		//
		// 	//Now, filling the form
		// 	emailField.setValue('ned_jakubowski@putsbox.com');
		// 	passwordField.setValue('automatedTest1');
		//
		// 	//Button must be enable now
		// 	expect(!!submitBtn.getAttribute('disabled')).to.equal(false);
		//
		// 	//Submits the form and wait
		// 	form.submitForm();
		// 	browser.waitForExist('.sc-kIPQKe.jVplDO',5000);
		//
		//
		// 	//Checks if we're in the correct URL.
		// 	const questionsURL = browser.getUrl();
		// 	expect(questionsURL).to.equal('https://dev.pointr.co/questions');
		//
		//
		// 	// Now it's time to sign out.
		//
		// 	//"Hi, First Name"
		// 	const HiFirstNameBtn = $('.sc-bxivhb.cTKtsz');
		// 	HiFirstNameBtn.click();
		//
		// 	//sign out button appears, clicks
		// 	const signOutBtn = $('.sc-bZQynM.bMmSDl');
		// 	signOutBtn.click();
		//
		//
		// 	//Sign In tab must be open: Modal exists and it is in the right URL.
		//
		// 	const signInURL = browser.getUrl();
		// 	expect(signInURL).to.equal('https://dev.pointr.co/register');
		//
		// 	expect(signInModal).to.exist;
		//
		// });
		//

	});
});
